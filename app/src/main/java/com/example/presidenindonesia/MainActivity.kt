package com.example.presidenindonesia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    var listPresiden= ArrayList<Presiden>()
    var adapter:Adapter?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listPresiden.add(
            Presiden("Ir. Soekarno","Presiden pertama Indonesia.",R.drawable.soekarno)
        )
        listPresiden.add(
            Presiden("Soeharto","Presiden kedua Indonesia",R.drawable.suharto)
        )
        listPresiden.add(
            Presiden("BJ Habibie","Presiden ketiga Indonesia",R.drawable.habibie)
        )
        listPresiden.add(
            Presiden("Abdurrahman Wahid","Presiden keempat Indonesia.",R.drawable.gusdur)
        )
        listPresiden.add(
            Presiden("Megawati Soekarnoputri","Presiden kelima Indonesia.",R.drawable.megawati)
        )
        listPresiden.add(
            Presiden("Susilo Bambang Yudhoyono","Presiden keenam Indonesia",R.drawable.sby)
        )
        listPresiden.add(
            Presiden("Ir. Joko Widodo","Presiden ketujuh Indonesia.",R.drawable.jokowi)
        )
        adapter= Adapter(listPresiden,this)
        listview.adapter=adapter
    }
}
